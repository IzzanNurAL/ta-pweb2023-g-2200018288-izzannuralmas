<!DOCTYPE html>
<html>
<head>
  <title>My Website - Project 2</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Welcome to my Homepage!</h1>
  </header>
  <nav>
    <ul>
      <li><a href="Home.php">Home</a></li>
      <li><a href="About.php">About</a></li>
      <li><a href="Contact.php">Contact</a></li>
    </ul>
    <div class="profile">
      <img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
      <h5>Izzan Nur Almas</h5>
    </div>
  </nav>
  <div class="conten">
    
    <div class="left-colom">
      <h2 id="my">My projects</h2>
      <ul class="projects">
        <li><a href="Home.php">Project 1</a></li>
        <li><a href="Beranda.php">Project 2</a></li>
        <li><a class="aktif" href="Form.php">Project 3</a></li>
      </ul>
    </div>
    <div class="right-colom">
      <div class="gmbratas">
        <nav id="silang">
          <button id="button" onclick="HapusIklan()">x</button>
        </nav>
        <h4 id="tulisan">Iklan</h4>
        <img id="iklan" src="https://rinhyukwook.files.wordpress.com/2013/04/xl-monyet-version-kompas-250-x-380-mmk1.jpg?w=394" alt="Iklan">
      </div>
      <div class="isiBiodata">
        <form method="post" action="hasil.php">
          <table>
            <hr><hr>
            <h1 align="center">Isi Biodata</h1>
            <hr>
            <tr>
              <td><label for="nama">Nama</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="nama" name="nama"><br></td>
            </tr>
            <tr>
              <td><label for="umur">Umur</label></td>
              <td class="titik">:</td>
              <td><input type="number" id="umur" name="umur"><br></td>
            </tr>
            <tr>
              <td><label for="kelamin">Jenis Kelamin</label></td>
              <td class="titik">:</td>
              <td>
                <input type="radio" id="kelamin" name="kelamin" value="laki-laki">Laki-laki
                <input type="radio" id="kelamin" name="kelamin" value="perempuan">Perempuan<br>
              </td>
            </tr>
            <tr>
              <td><label for="alamat">Alamat</label></td>
              <td class="titik">:</td>
              <td><textarea id="alamat" name="alamat"></textarea><br></td>
            </tr>
            <tr>
              <td><label for="pekerjaan">Pekerjaan</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="pekerjaan" name="pekerjaan"><br></td>
            </tr>
            <tr>
              <td><label for="email">Email</label></td>
              <td class="titik">:</td>
              <td><input type="email" id="email" name="email"><br></td>
            </tr>
            <tr>
              <td><label for="telepon">Telepon</label></td>
              <td class="titik">:</td>
              <td><input type="tel" id="telepon" name="telepon"><br></td>
            </tr>
            
          </table>
          <hr><hr>
          <input type="submit" value="Kirim" onclick="Kirim()">
        </form>
      </div>

      
    </div>
    
  </div>
  <div>
    <img class="gambar" src="https://pptqahmaddahlancaruban.com/wp-content/uploads/2021/07/Eid_Aldha_Background_02-1290x540.jpg">
  </div>
  <footer>
    <p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
  </footer>

  <script type="text/javascript" src="script.js"></script>
</body>
</html>