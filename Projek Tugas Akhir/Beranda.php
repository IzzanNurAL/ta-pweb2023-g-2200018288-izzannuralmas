<!DOCTYPE html>
<html>
<head>
  <title>My Website - Project 2</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Welcome to my Homepage!</h1>
  </header>
  <nav>
    <ul>
      <li><a href="Home.php">Home</a></li>
      <li><a href="About.php">About</a></li>
      <li><a href="Contact.php">Contact</a></li>
    </ul>
    <div class="profile">
      <img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
      <h5>Izzan Nur Almas</h5>
    </div>
  </nav>
  <div class="conten">
    
    <div class="left-colom">
      <h2 id="my">My projects</h2>
      <ul class="projects">
        <li><a href="Home.php">Project 1</a></li>
        <li><a class="aktif" href="Beranda.php">Project 2</a></li>
        <li><a href="Form.php">Project 3</a></li>
      </ul>
    </div>
    <div class="right-colom">
      <h1>Perpustakaan Kabupaten Brebes</h1>
      <hr>
      <h4>Halaman Web :</h4>
      <ul>
        <li><a href="Beranda.php">Beranda</a></li>
        <li><a href="DataBuku.php">Daftar Buku</a></li>
        <li><a href="Tanya.php">Tanya</a></li>
      </ul>
      <hr>
      <h1>Beranda</h1>
      <h2>Selamat datang di Perpustakaan Daerah Kabupaten Brebes</h2>
      <p>
        Perpustakaan ini diperuntukan untuk siapa saja yang ingin membaca buku-buku populer seputar sains, fiksi dan ilmu sosial.
      </p>
    </div>
    
  </div>
  <div>
    <img class="gambar" src="https://pptqahmaddahlancaruban.com/wp-content/uploads/2021/07/Eid_Aldha_Background_02-1290x540.jpg">
  </div>
  <footer>
    <p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
  </footer>

</body>
</html>