<!DOCTYPE html>
<html>
<head>
  <title>My Website - Project 2</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Welcome to my Homepage!</h1>
  </header>
  <nav>
    <ul>
      <li><a href="Home.php">Home</a></li>
      <li><a class="aktif" href="About.php">About</a></li>
      <li><a href="Contact.php">Contact</a></li>
    </ul>
    <div class="profile">
      <img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
      <h5>Izzan Nur Almas</h5>
    </div>
  </nav>
  <div class="about">
     	<h2>Program apa ini ?</h2>
      <p>Program ini adalah halaman web untuk situs perpustakaan Kabupaten Brebes. Halaman ini memiliki tampilan yang rapi dan terdiri dari beberapa bagian utama. Header terletak di bagian atas dengan judul "Welcome to my Homepage!". Navigasi terdiri dari tiga menu: Home, About, dan Contact yang berada di sebelah kiri. Sedangkan di sebelah kanan terdapat Profile yang terdiri dari Foto dan Nama.</p>

      <p>Konten utama terdiri dari dua kolom. Kolom kiri berisi daftar proyek dengan judul "My projects" dan tiga proyek yang dapat diklik. Kolom kanan berisi informasi tentang perpustakaan, termasuk daftar halaman web seperti Beranda, Daftar Buku, dan Tanya untuk mengirim pertanyaan. </p>

      <p>Di bagian bawah halaman terdapat sebuah gambar yang ditampilkan secara penuh. Footer menampilkan informasi hak cipta.</p>

      <p>Program ini menggunakan php dan CSS untuk mengatur tata letak dan gaya visual halaman web. Beberapa elemen juga menggunakan JavaScript untuk menambahkan interaktivitas seperti perubahan warna saat dihover.</p>

      <p>Dengan struktur yang rapi dan desain yang sederhana, halaman web ini memberikan pengguna pengalaman yang mudah dipahami dan menyenangkan saat mengakses situs perpustakaan Kabupaten Brebes.</p>

      <p>
        <br><br><br><br><br><br><br><br><br><br><br><br>
      </p>
  </div>
  <footer>
    <p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
  </footer>

</body>
</html>