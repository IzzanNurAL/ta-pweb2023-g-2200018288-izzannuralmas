<!DOCTYPE html>
<html>
<head>
  <title>My Website - Project 2</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  
  <header>
    <h1>Welcome to my Homepage!</h1>
  </header>
  <nav>
    <ul>
      <li><a href="Home.php">Home</a></li>
      <li><a href="About.php">About</a></li>
      <li><a href="Contact.php">Contact</a></li>
    </ul>
    <div class="profile">
      <img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
      <h5>Izzan Nur Almas</h5>
    </div>
  </nav>
  <div class="conten">
    
    <div class="left-colom">
      <h2 id="my">My projects</h2>
      <ul class="projects">
        <li><a href="Home.php">Project 1</a></li>
        <li><a href="Beranda.php">Project 2</a></li>
        <li><a class="aktif" href="Form.php">Project 3</a></li>
      </ul>
    </div>
    <div class="right-colom">
      <div class="gmbratas">
        <nav id="silang">
          <button id="button" onclick="HapusIklan()">x</button>
        </nav>
        <h4 id="tulisan">Iklan</h4>
        <img id="iklan" src="https://rinhyukwook.files.wordpress.com/2013/04/xl-monyet-version-kompas-250-x-380-mmk1.jpg?w=394" alt="Iklan">
      </div>
      <div class="isiBiodata">
        <?php
        if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["nama"] && $_POST["umur"] && $_POST["kelamin"] && $_POST["alamat"] && $_POST["pekerjaan"] && $_POST["email"]) && $_POST["telepon"]){
          $nama = $_POST["nama"];
          $umur = $_POST["umur"];
          $kelamin = $_POST["kelamin"];
          $alamat = $_POST["alamat"];
          $pekerjaan = $_POST["pekerjaan"];
          $email = $_POST["email"];
          $telepon = $_POST["telepon"];

          echo "<table>
                  <hr><hr>
                  <h1>Biodata Anda</h1>
                  <hr>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>" . $nama . "<br></td>
                  </tr>
                  <tr>
                    <td>Umur</td>
                    <td>:</td>
                    <td>" . $umur . "<br></td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>" . $kelamin . "<br></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>" . $alamat . "<br></td>
                  </tr>
                  <tr>
                    <td>Pekerjaan</td>
                    <td>:</td>
                    <td>" . $pekerjaan . "<br></td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>" . $email . "<br></td>
                  </tr>
                  <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td>" . $telepon . "<br></td>
                  </tr>
                  
                </table>
                <hr><hr>";
        }
        ?>
        <a  href="Form.php" class="tombol">Kembali</a>
      </div>
      
    </div>
    
  </div>
  <div>
    <img class="gambar" src="https://pptqahmaddahlancaruban.com/wp-content/uploads/2021/07/Eid_Aldha_Background_02-1290x540.jpg">
  </div>
  <footer>
    <p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
  </footer>

  <script type="text/javascript" src="script.js"></script>
</body>
</html>