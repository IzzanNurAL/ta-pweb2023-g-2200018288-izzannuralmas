<!DOCTYPE html>
<html>
<head>
	<title>My Website</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>
		<h1>Welcome to my Homepage!</h1>
	</header>
	<nav>
		<ul>
			<li><a class="aktif" href="Home.php">Home</a></li>
			<li><a href="About.php">About</a></li>
			<li><a href="Contact.php">Contact</a></li>
		</ul>
		<div class="profile">
			<img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
			<h5>Izzan Nur Almas</h5>
		</div>
	</nav>
	<div class="conten">
		
		<div class="left-colom">
			<h2 id="my">My projects</h2>
			<ul class="projects">
				<li><a class="aktif" href="Home.php">Project 1</a></li>
				<li><a href="Beranda.php">Project 2</a></li>
				<li><a href="Form.php">Project 3</a></li>
			</ul>
		</div>
		<div class="right-colom">
			<h2>السَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ اللهِ وَبَرَكَاتُهُ</h2>
			<p>Hi, my name is Izzan Nur A. I'm a web developer based in Brebes City.</p>
			
				<hr><hr>
				<h1>Biodata</h1>
				<hr>
			<table>
				<tr>
					<td>Nama</td>
					<td class="titik">:</td>
					<td>Izzan Nur Almas</td>
				</tr>
				<tr>
					<td>Pekerjaan</td>
					<td class="titik">:</td>
					<td>Mahasiswa</td>
				</tr>
				<tr>
					<td>Usia</td>
					<td class="titik">:</td>
					<td>19 Tahun</td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td class="titik">:</td>
					<td>Laki-laki</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td class="titik">:</td>
					<td>Brebes, Jawa Tengah, Indonesia</td>
				</tr>
				<tr>
					<td>Email</td>
					<td class="titik">:</td>
					<td>izzan@gmail.com</td>
				</tr>
				
			</table>
			<hr><hr>

			<button id="tombol" onclick="color()">Ganti</button>
			
		</div>
		
	</div>
	<div>
		<img class="gambar" src="https://pptqahmaddahlancaruban.com/wp-content/uploads/2021/07/Eid_Aldha_Background_02-1290x540.jpg">
	</div>
	<footer>
		<p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
	</footer>

	<script type="text/javascript" src="script.js"></script>	
</body>
</html>