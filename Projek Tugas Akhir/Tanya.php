<!DOCTYPE html>
<html>
<head>
  <title>My Website - Project 2</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <header>
    <h1>Welcome to my Homepage!</h1>
  </header>
  <nav>
    <ul>
      <li><a href="Home.php">Home</a></li>
      <li><a href="About.php">About</a></li>
      <li><a href="Contact.php">Contact</a></li>
    </ul>
    <div class="profile">
      <img class="fotoProfile" src="https://drive.google.com/uc?export=view&id=17NgGXytmTB3F-XIdXFpIb6z_KU4N5Fam" alt="Gambar Profile">
      <h5>Izzan Nur Almas</h5>
    </div>
  </nav>
  <div class="conten">
    <div class="left-colom">
      <h2 id="my">My projects</h2>
      <ul class="projects">
        <li><a href="Home.php">Project 1</a></li>
        <li><a class="aktif" href="Beranda.php">Project 2</a></li>
        <li><a href="Form.php">Project 3</a></li>
      </ul>
    </div>
    <div class="right-colom">
      <h1>Perpustakaan Kabupaten Brebes</h1>
    <hr>
    <h4>Halaman Web :</h4>
    <ul>
      <li><a href="Beranda.php">Beranda</a></li>
      <li><a href="DataBuku.php">Daftar Buku</a></li>
      <li><a href="Tanya.php">Tanya</a></li>
    </ul>
    <hr>
    <h1>Ajukan Pertanyaan<br><br></h1>

    <?php
      // Inisialisasi variabel dengan nilai awal kosong
      $nameErr = $emailErr = $alamatErr = $teleponErr = $messageErr = "";
      $name = $email = $alamat = $telepon = $message = "";

      // Fungsi untuk membersihkan dan memvalidasi input
      function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }

      // Memproses data setelah formulir disubmit
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Validasi nama
        if (empty($_POST["data_nama"])) {
          $nameErr = "Nama harus diisi";
        } else {
          $name = test_input($_POST["data_nama"]);
          // Validasi hanya huruf dan spasi
          if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            $nameErr = "Hanya diperbolehkan huruf dan spasi";
          }
        }

        // Validasi email
        if (empty($_POST["email"])) {
          $emailErr = "Email harus diisi";
        } else {
          $email = test_input($_POST["email"]);
          // Validasi format email
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Format email tidak valid";
          }
        }

        // Validasi alamat
        if (empty($_POST["alamat"])) {
          $alamatErr = "Alamat harus diisi";
        } else {
          $alamat = test_input($_POST["alamat"]);
        }

        // Validasi nomor telepon
        if (empty($_POST["telepon"])) {
          $teleponErr = "Nomor telepon harus diisi";
        } else {
          $telepon = test_input($_POST["telepon"]);
          // Validasi hanya angka
          if (!preg_match("/^[0-9]*$/", $telepon)) {
            $teleponErr = "Hanya diperbolehkan angka";
          }
        }

        // Validasi pesan
        if (empty($_POST["data_tanya"])) {
          $messageErr = "Pesan harus diisi";
        } else {
          $message = test_input($_POST["data_tanya"]);
        }
      }
    ?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
      <table>
            <tr>
              <td><label for="nama">Nama</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="data_nama" name="data_nama" value="<?php echo $name; ?>"> <span class="error"><?php echo $nameErr; ?></span><br></td>
            </tr>
            <tr>
              <td><label for="email">Email</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="email" name="email" value="<?php echo $email; ?>"> <span class="error"><?php echo $emailErr; ?></span><br></td>
            </tr>
            <tr>
              <td><label for="alamat">Alamat</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="alamat" name="alamat" value="<?php echo $alamat; ?>"> <span class="error"><?php echo $alamatErr; ?></span><br></td>
            </tr>
            <tr>
              <td><label for="telepon">Nomor Telepon</label></td>
              <td class="titik">:</td>
              <td><input type="text" id="telepon" name="telepon" value="<?php echo $telepon; ?>"> <span class="error"><?php echo $teleponErr; ?></span><br></td>
            </tr>
            <tr>
              <td><label for="data_tanya">Pertanyaan</label></td>
              <td class="titik">:</td>
              <td><textarea name="data_tanya" id="data_tanya" cols="30" rows="10" placeholder="Isi pertanyaan di sini"><?php echo $message; ?></textarea> <span class="error"><?php echo $messageErr; ?></span><br></td>
            </tr>
      </table>
      <input type="submit" value="Kirim">
    </form>
    <?php
      // Menampilkan output jika validasi terpenuhi
      if ($_SERVER["REQUEST_METHOD"] == "POST" && $nameErr == "" && $emailErr == "" && $alamatErr == "" && $teleponErr == "" && $messageErr == "") {


        echo "<table>
                  <hr>
                  <h1>Terima kasih telah bertanya</h1><br>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>" . $name . "<br></td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>" . $email . "<br></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>" . $alamat . "<br></td>
                  </tr>
                  <tr>
                    <td>Nomor Telepon</td>
                    <td>:</td>
                    <td>" . $telepon . "<br></td>
                  </tr>
                  <tr>
                    <td>Pesan</td>
                    <td>:</td>
                    <td>" . $message . "<br></td>
                  </tr>
                </table>";
      }
    ?>
    </div>
    
  </div>
  <div>
    <img class="gambar" src="https://pptqahmaddahlancaruban.com/wp-content/uploads/2021/07/Eid_Aldha_Background_02-1290x540.jpg">
  </div>
  <footer>
    <p>&copy; 2023 Izzan Nur A. All rights reserved.</p>
  </footer>

  <script type="text/javascript" src="script.js"></script>
</body>
</html>