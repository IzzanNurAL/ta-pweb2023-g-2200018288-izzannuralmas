<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body style="background-color: orange;">
<div style="text-align: right; font-family: arial;">Izzan Nur Almas</div>
<div style="padding: 0% 30%;" align="center">
	<h1 style="text-align: center; color: darkblue;">PHP DASAR</h1>
	<h2 style="text-align: left; padding-left: 20%;">
		<?php

		$gaji = 1000000;
		$pajak = 0.1;
		$thp = $gaji - ($gaji*$pajak);

		echo "Gaji sebelum pajak = Rp. $gaji <br>";
		echo "Gaji yang dibawa pulang = Rp. $thp<br>";
		?>

		<?php

		$a = 5;
		$b = 4;

		echo "<br>$a == $b : ". ($a == $b);
		echo "<br>$a != $b : ". ($a != $b);
		echo "<br>$a > $b : ". ($a > $b);
		echo "<br>$a < $b : ". ($a < $b);
		echo "<br>($a == $b) && ($a > $b) : ".(($a == $b) && ($a > $b));
		echo "<br>($a == $b) || ($a > $b) : ".(($a == $b) || ($a > $b));

		?>
	</h2>
</div>
</body>
</html>
