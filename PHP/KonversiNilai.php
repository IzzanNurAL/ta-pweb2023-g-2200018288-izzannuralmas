<!DOCTYPE html>
<html>
<head>
  <title>Konversi Nilai</title>
</head>
<body>
  <form method="POST">
    <label for="nilai">Masukkan nilai angka:</label>
    <input type="text" id="nilai" name="nilaiAngka" value="<?php echo isset($_POST["nilaiAngka"]) ? $_POST["nilaiAngka"] : ''; ?>">
    <button type="submit">Konversi</button>
  </form>
  
</body>
</html>

<?php
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["nilaiAngka"]) && !empty($_POST["nilaiAngka"])) {
      $nilaiAngka = floatval($_POST["nilaiAngka"]);

      // Lakukan konversi nilai huruf dan tampilkan hasil
      // Fungsi untuk konversi nilai angka ke nilai huruf
        function konversiNilaiHuruf($nilaiAngka) {
            if ($nilaiAngka >= 80.00 && $nilaiAngka <= 100.00) {
                return "A";
            } elseif ($nilaiAngka >= 76.25 && $nilaiAngka <= 79.99) {
                return "A-";
            } elseif ($nilaiAngka >= 68.75 && $nilaiAngka <= 76.24) {
                return "B+";
            } elseif ($nilaiAngka >= 65.00 && $nilaiAngka <= 68.74) {
                return "B";
            } elseif ($nilaiAngka >= 62.50 && $nilaiAngka <= 64.99) {
                return "B-";
            } elseif ($nilaiAngka >= 57.50 && $nilaiAngka <= 62.49) {
                return "C+";
            } elseif ($nilaiAngka >= 55.00 && $nilaiAngka <= 57.49) {
                return "C";
            } elseif ($nilaiAngka >= 51.25 && $nilaiAngka <= 54.99) {
                return "C-";
            } elseif ($nilaiAngka >= 43.75 && $nilaiAngka <= 51.24) {
                return "D+";
            } elseif ($nilaiAngka >= 40.00 && $nilaiAngka <= 43.74) {
                return "D";
            } elseif ($nilaiAngka >= 0.00 && $nilaiAngka <= 39.99) {
                return "E";
            } else {
                return "Nilai angka tidak valid.";
            }
        }

        // Memanggil fungsi konversiNilaiHuruf dan menampilkan hasil
        $nilaiHuruf = konversiNilaiHuruf($nilaiAngka);
        echo "Nilai huruf: " . $nilaiHuruf;
    } else {
      echo "Mohon masukkan nilai angka.";
    }
  }
  ?>