<!DOCTYPE html>
<html>
<head>
  <title>Segitiga Bintang</title>
</head>
<body>
  <form method="POST">
    <label for="input">Masukkan tinggi segitiga:</label>
    <input type="text" id="input" name="tinggi">
    <button type="submit">Buat Segitiga</button>
  </form>
</body>
</html>

<?php
if (isset($_POST["tinggi"])) {
  $tinggi = intval($_POST["tinggi"]);
  if ($tinggi > 0) {
    // code...
    
    // Fungsi untuk membuat segitiga bintang
    function createTriangle($tinggi) {
      $output = '';
      for ($baris = 1; $baris <= $tinggi; $baris++) {
        // Buat sejumlah spasi
        for ($i = 1; $i <= $tinggi - $baris; $i++) {
          $output .= " ";
        }
        // Tampilkan *
        for ($j = 1; $j < 2 * $baris; $j++) {
          $output .= "*";
        }
        // Pindah baris
        $output .= "\n";
      }
      return $output;
    }
    $triangle = createTriangle($tinggi);
    echo "<pre>" . $triangle . "</pre>";
  }
  else {
    echo "<pre>Inputkan angka > 0 (positif).</pre>";
  }
}
?>